package com.fourcatsdev.cruddtoservice.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fourcatsdev.cruddtoservice.exception.ProdutoNotFoundException;
import com.fourcatsdev.cruddtoservice.modelo.Produto;
import com.fourcatsdev.cruddtoservice.repository.ProdutoRepository;

@Service
public class ProdutoService {

	@Autowired
	private ProdutoRepository produtoRepository;
	
	public Produto criarProduto(Produto produto) {
		return produtoRepository.save(produto);
	}

	public Produto buscarProdutoPorId(Long id) throws ProdutoNotFoundException {
		Optional<Produto> opt = produtoRepository.findById(id);
		if (opt.isPresent()) {
			return opt.get();
		} else {
			throw new ProdutoNotFoundException("Produto com id : " + id + " não existe");
		}
	}

	public List<Produto> buscarTodosProdutos() {
		return produtoRepository.findAll();
	}

	public Produto alterarProduto(Produto produto) {
		return produtoRepository.save(produto);
	}

	public void apagarProduto(Long id) throws ProdutoNotFoundException {
		Produto produto = buscarProdutoPorId(id);
		produtoRepository.delete(produto);
	}
}
