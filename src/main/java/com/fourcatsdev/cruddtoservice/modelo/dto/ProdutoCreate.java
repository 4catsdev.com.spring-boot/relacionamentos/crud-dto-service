package com.fourcatsdev.cruddtoservice.modelo.dto;

import java.math.BigDecimal;

import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

public class ProdutoCreate {

	@NotBlank(message = "A descrição deve ser informada.")
	@Size(min = 3, max = 30, message = "A descrição deve ter entre 3 e 30 caracteres.")
	private String descricao;
	
	@DecimalMin(value = "0.0", inclusive = false, message = "O valor deve ser maior ou igual a 0.")
	private BigDecimal valor;
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
}
