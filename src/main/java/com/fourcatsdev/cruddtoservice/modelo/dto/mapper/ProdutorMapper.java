package com.fourcatsdev.cruddtoservice.modelo.dto.mapper;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import com.fourcatsdev.cruddtoservice.modelo.Produto;
import com.fourcatsdev.cruddtoservice.modelo.dto.ProdutoCreate;
import com.fourcatsdev.cruddtoservice.modelo.dto.ProdutoResponse;

public class ProdutorMapper {
	
	public static Produto toEntity(ProdutoCreate dto) {
		Produto produto = new Produto();
		produto.setDescricao(dto.getDescricao());
		produto.setValor(dto.getValor().doubleValue());
		
		return produto;		
	}
	
	public static Produto toEntity(ProdutoResponse dto) {
		Produto produto = new Produto();
		produto.setDescricao(dto.getDescricao());
		produto.setValor(dto.getValor().doubleValue());
		produto.setId(dto.getId());
		
		return produto;		
	}
	
	public static ProdutoResponse toDTO(Produto produto) {
		ProdutoResponse dto = new ProdutoResponse();
		dto.setId(produto.getId());
		dto.setDescricao(produto.getDescricao());
		dto.setValor(BigDecimal.valueOf(produto.getValor()));
		
		return dto;		
	}
	
	public static List<ProdutoResponse> toDTO(List<Produto> produtos) {
		return produtos.stream()
        		.map(produto -> toDTO(produto))
                .collect(Collectors.toList());
    }

}
