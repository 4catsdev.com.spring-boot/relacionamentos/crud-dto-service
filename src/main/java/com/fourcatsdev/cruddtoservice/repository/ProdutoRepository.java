package com.fourcatsdev.cruddtoservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fourcatsdev.cruddtoservice.modelo.Produto;

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Long> {

}
