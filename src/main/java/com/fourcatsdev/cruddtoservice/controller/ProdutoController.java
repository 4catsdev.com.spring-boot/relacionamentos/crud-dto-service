package com.fourcatsdev.cruddtoservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fourcatsdev.cruddtoservice.exception.ProdutoNotFoundException;
import com.fourcatsdev.cruddtoservice.modelo.Produto;
import com.fourcatsdev.cruddtoservice.modelo.dto.ProdutoCreate;
import com.fourcatsdev.cruddtoservice.modelo.dto.ProdutoResponse;
import com.fourcatsdev.cruddtoservice.modelo.dto.mapper.ProdutorMapper;
import com.fourcatsdev.cruddtoservice.service.ProdutoService;

import jakarta.validation.Valid;

@Controller
public class ProdutoController {
	
	@Autowired
	private ProdutoService produtoService;

	@GetMapping("/")
    public String listarProdutos(Model model) {
		List<Produto> produtos = produtoService.buscarTodosProdutos();
        List<ProdutoResponse> produtosDto = ProdutorMapper.toDTO(produtos);
        model.addAttribute("produtos", produtosDto);
        return "/produtos";
    }
	
	@GetMapping("/novo")
    public String novoProduto(Model model) {
		model.addAttribute("item", new ProdutoCreate());
        return "/novo";
    }
	
	@PostMapping("/criar")
    public String gravarProduto(@ModelAttribute("item") @Valid ProdutoCreate produtoCreate, 
    							BindingResult result, RedirectAttributes attributes) {
		if (result.hasErrors()) {
            return "novo";
        }
		Produto produto = ProdutorMapper.toEntity(produtoCreate);
        produtoService.criarProduto(produto);
        attributes.addFlashAttribute("mensagem", "Gravado com sucesso");
        return "redirect:/";
    }
	
	@GetMapping("/editar/{id}")
    public String showFormEditar(@PathVariable("id") long id, Model model, 
								 RedirectAttributes attributes) {
		try {
			Produto produto = produtoService.buscarProdutoPorId(id);
			ProdutoResponse produtoResponse = ProdutorMapper.toDTO(produto);
			model.addAttribute("item", produtoResponse);
		} catch (ProdutoNotFoundException e) {
			attributes.addFlashAttribute("mensagem", e.getMessage());
			return "redirect:/";
		}
		
		return "/editar";
    }
	
	@PostMapping("/editar/{id}")
	public String editarProduto(@PathVariable("id") long id, 
								@ModelAttribute("item") @Valid ProdutoResponse produtoResponse, 
								BindingResult result) {
		if (result.hasErrors()) {
	    	produtoResponse.setId(id);
	        return "/editar";
	    }		
		Produto produto = ProdutorMapper.toEntity(produtoResponse);
        produtoService.alterarProduto(produto);
	    return "redirect:/";
	}
	
	@GetMapping("/apagar/{id}")
	public String ApagarProduto(@PathVariable("id") long id, 
								RedirectAttributes attributes) {
		try {
			produtoService.apagarProduto(id);
		} catch (ProdutoNotFoundException e) {
			attributes.addFlashAttribute("mensagem", e.getMessage());
		}
	    return "redirect:/";
	}
}
